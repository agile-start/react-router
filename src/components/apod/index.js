import React from 'react';
import PropTypes from 'prop-types';

import MediaImage from '../media-image';
import MediaVideo from '../media-video';

import './style.scss';

const Apod = ({ data, onLoadImage, className }) => {
  const {
    copyright, explanation, title, url, media_type: mediaType,
  } = data;

  return (
    <div className={`component-apod ${className}`}>
      <h2>{title}</h2>

      {
        mediaType === 'video'
          ? <MediaVideo url={url} copyright={copyright} onLoad={onLoadImage} />
          : <MediaImage url={url} copyright={copyright} onLoad={onLoadImage} />
      }

      <p>{explanation}</p>
    </div>
  );
};

Apod.propTypes = {
  data: PropTypes.shape({
    copyright: PropTypes.string,
    explanation: PropTypes.string,
    media_type: PropTypes.string,
    title: PropTypes.string,
    url: PropTypes.string,
  }).isRequired,
  className: PropTypes.string,
  onLoadImage: PropTypes.func,
};

Apod.defaultProps = {
  className: '',
  onLoadImage: () => {},
};

export default Apod;
