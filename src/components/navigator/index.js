import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import './style.scss';

const Navigator = ({ date, className }) => {
  const oneDay = 86400000;
  const today = new Date();
  let now = today;

  if (date !== null) {
    const splitedDate = date.split('-');
    now = new Date(splitedDate[0], splitedDate[1] - 1, splitedDate[2]);
  }

  const backward = new Date(now.getTime() - oneDay);
  const forward = new Date(now.getTime() + oneDay);

  const backwardLink = `${backward.getFullYear()}-${backward.getMonth() + 1}-${backward.getDate()}`;
  const todayLink = `${today.getFullYear()}-${today.getMonth() + 1}-${today.getDate()}`;
  const forwardLink = `${forward.getFullYear()}-${forward.getMonth() + 1}-${forward.getDate()}`;

  return (
    <div className={`component-navigator ${className}`}>
      <Link to={backwardLink} className="component-navigator__button"> &lt;&lt; </Link>
      <Link to={todayLink} className="component-navigator__button"> today </Link>
      <Link to={forwardLink} className="component-navigator__button"> &gt;&gt; </Link>
    </div>
  );
};

Navigator.propTypes = {
  date: PropTypes.string,
  className: PropTypes.string,
};

Navigator.defaultProps = {
  date: null,
  className: '',
};

export default Navigator;
