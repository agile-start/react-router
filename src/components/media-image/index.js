import React from 'react';
import PropTypes from 'prop-types';

import './style.scss';

const MediaImage = ({
  url, copyright, onLoad, className,
}) => {
  return (
    <figure className={`component-media-image ${className}`}>
      <img src={url} alt="Of the day" onLoad={onLoad} />
      <figcaption>
          Copyright: {copyright}
      </figcaption>
    </figure>
  );
};

MediaImage.propTypes = {
  url: PropTypes.string.isRequired,
  copyright: PropTypes.string,
  className: PropTypes.string,
  onLoad: PropTypes.func,
};

MediaImage.defaultProps = {
  copyright: '',
  className: '',
  onLoad: () => {},
};

export default MediaImage;
