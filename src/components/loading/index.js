import React from 'react';
import PropTypes from 'prop-types';

import imgLoading from './images/loading.gif';
import './style.scss';

const Loading = ({ className }) => (
  <div className={`component-loading ${className}`}>
    <img src={imgLoading} alt="loading" />
  </div>
);

Loading.propTypes = {
  className: PropTypes.string
};

Loading.propValues = {
  className: ''
};

export default Loading;
