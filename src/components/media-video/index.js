import React from 'react';
import PropTypes from 'prop-types';

import './style.scss';

const MediaVideo = ({
  url, onLoad, className,
}) => {
  return (
    <div className={`component-media-video ${className}`}>
      <iframe src={url} title="apod video" onLoad={onLoad} />
    </div>
  );
};

MediaVideo.propTypes = {
  url: PropTypes.string.isRequired,
  className: PropTypes.string,
  onLoad: PropTypes.func,
};

MediaVideo.defaultProps = {
  className: '',
  onLoad: () => {},
};

export default MediaVideo;
