import React from 'react';

import Header from '../../components/header';
import Footer from '../../components/footer';
import Router from '../../router';

import './style.scss';

const PageMain = () => (
  <div className="page-main">
    <Header />
    <Router />
    <Footer />
  </div>
);

export default PageMain;
