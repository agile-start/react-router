import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

import Page from './pages/main';

import registerServiceWorker from './registerServiceWorker';

render(
  <BrowserRouter>
    <Page />
  </BrowserRouter>,

  document.getElementById('root'),
);

registerServiceWorker();
