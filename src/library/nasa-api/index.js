import axios from 'axios';

import env from '../../config/env';

export function apod ({ date }) {
  let apiUrl = `${env.REACT_APP_NASA_API_HOST}planetary/apod?api_key=${env.REACT_APP_NASA_API_KEY}`;

  if (date) apiUrl += `&date=${date}`;

  return axios.get(apiUrl)
    .then(dataRequest => dataRequest.data);
}

export default {
  apod,
};
