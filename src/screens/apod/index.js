import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import Apod from '../../components/apod';
import Loading from '../../components/loading';
import Navigator from '../../components/navigator';

import { apod as nasaApod } from '../../library/nasa-api';

import './style.scss';

class ScreenApod extends React.Component {
  static propTypes = {
    match: PropTypes.object.isRequired,
  };

  static getDerivedStateFromProps (nextProps, prevState) {
    if (nextProps.match.params.date !== prevState.date) {
      return { date: nextProps.match.params.date };
    }

    return null;
  }

  constructor (props) {
    super(props);

    this.state = {
      apod: {},
      loading: true,
      erro: false,
      date: props.match.params.date || '',
    };
  }


  // componentWillMount () {
  //   this.loadApod();
  // }

  componentDidMount () {
    this.loadApod();
  }

  componentDidUpdate (prevProps, prevState) {
    const { state } = this;

    if (state.date !== prevState.date) {
      this.loadApod();
    }
  }

  loadApod () {
    this.setState({ loading: true, erro: false });

    const { state } = this;

    nasaApod({ date: state.date })
      .then((data) => {
        this.setState({ apod: data, erro: false });
      })
      .catch(() => {
        this.setState({ loading: false, erro: true });
      });
  }

  updateDone () {
    this.setState({ loading: false });
  }


  render () {
    const {
      date, apod, loading, erro,
    } = this.state;

    const pageClassNames = {
      'screen-apod': true,
      loading,
      erro,
    };

    return (
      <div className={classNames(pageClassNames)}>
        <Navigator
          date={date}
        />
        <Apod data={apod} className="screen-apod__apod" onLoadImage={() => this.updateDone()} />
        <Loading className="screen-apod__loading" />
        <div className="screen-apod__erro">Content not found for the date</div>
      </div>
    );
  }
}

export default ScreenApod;
